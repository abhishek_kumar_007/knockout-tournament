import React from "react";
import KnockoutFixtures from "../../containers/KnockoutFixtures/KnockoutFixtures";

function KnockoutFixturesPage() {
  return <KnockoutFixtures />;
}

export default KnockoutFixturesPage;
