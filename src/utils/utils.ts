export function genRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * max + min);
}
