import { genRandomNumber } from "../utils/utils";
import { Team, TeamsApiResponse } from "../communicators/Teams";

export interface RoundScoreCard {
  matchId: number;
  team1: Team;
  team2: Team;
  teamWonIndex: -1 | 1 | 2; // -1 : noResult, 1: team1 won, 2: team2 won
  isMatchInProgress: boolean;
  isMatchComplete: boolean;
}

export type ScoreCard = Array<Array<RoundScoreCard>>;

export interface ScoreCardOptions {
  scoreCard: ScoreCard;
}

class ScoreCardModel {
  _scoreCard: ScoreCard;

  constructor({ scoreCard }: ScoreCardOptions) {
    this._scoreCard = scoreCard || [];
  }

  static getMatchConfigs = (nTeam: number) => {
    let pow = 0;

    while (Math.pow(2, pow) < nTeam) {
      pow++;
    }

    const totalPossibleTeams = Math.pow(2, pow);
    const byes = totalPossibleTeams - nTeam;

    return {
      byes,
      nRounds: pow,
      totalPossibleGroups: totalPossibleTeams / 2,
      nByesInUpperHalf: (byes % 2 === 0 ? byes : byes - 1) / 2,
      nByesInLowerHalf: (byes % 2 === 0 ? byes : byes + 1) / 2,
      nMatches: nTeam - 1,
      nTeamsInUpperHalf: (nTeam % 2 === 0 ? nTeam : nTeam + 1) / 2,
      nTeamsInLowerHalf: (nTeam % 2 === 0 ? nTeam : nTeam - 1) / 2
    };
  };

  static updateScoreCard = (scoreCard: ScoreCard, currentRound: number) => {
    const nextRound = currentRound + 1;

    if (Array.isArray(scoreCard) && scoreCard.length > nextRound) {
      const roundMatches = scoreCard[currentRound];
      const winners: Array<Team> = [];
      roundMatches.forEach(matches => {
        if (matches.isMatchComplete && matches.teamWonIndex !== -1) {
          winners.push(
            matches.teamWonIndex === 1 ? matches.team1 : matches.team2
          );
        }
      });

      let counter = 0;
      const nextScoreCard = scoreCard[nextRound];
      const rounds = [];
      for (let j = 0; j < nextScoreCard.length; j++) {
        rounds.push({
          ...nextScoreCard[j],
          team1: winners[counter++],
          team2: winners[counter++]
        });
      }

      scoreCard[nextRound] = rounds;
    }

    return scoreCard;
  };

  static generateInitialScoreCard = (
    nTeams: number,
    teamData: TeamsApiResponse
  ) => {
    const config = ScoreCardModel.getMatchConfigs(nTeams);

    const scoreCard: ScoreCard = [];
    const teamsWithByes: TeamsApiResponse = [];

    let counter = 0;
    for (let i = 1; i <= config.totalPossibleGroups * 2; i++) {
      if (i <= config.totalPossibleGroups) {
        if (i % 2 === 0 && i <= config.nByesInUpperHalf * 2) {
          teamsWithByes.push({
            teamId: Date.now(),
            teamName: "Bye",
            countryCode: "BYE"
          });
        } else {
          teamsWithByes.push(teamData[counter++]);
        }
      } else {
        if (
          i % 2 === 0 &&
          i <= config.totalPossibleGroups + config.nByesInLowerHalf * 2
        ) {
          teamsWithByes.push({
            teamId: Date.now(),
            teamName: "Bye",
            countryCode: "BYE"
          });
        } else {
          teamsWithByes.push(teamData[counter++]);
        }
      }
    }

    let totalGroupsInRound = config.totalPossibleGroups;
    counter = 0;

    let matchCounter = 0;
    for (let i = 0; i < config.nRounds; i++) {
      const roundMatches: Array<RoundScoreCard> = [];

      for (let j = 0; j < totalGroupsInRound; j++) {
        if (i === 0) {
          // round1
          roundMatches.push({
            matchId: matchCounter++,
            team1: teamsWithByes[counter++],
            team2: teamsWithByes[counter++],
            teamWonIndex: -1,
            isMatchInProgress: false,
            isMatchComplete: false
          });
        } else {
          roundMatches.push({
            matchId: matchCounter++,
            team1: { teamId: Date.now(), countryCode: "BYE", teamName: "TBD" },
            team2: { teamId: Date.now(), countryCode: "BYE", teamName: "TBD" },
            teamWonIndex: -1,
            isMatchInProgress: false,
            isMatchComplete: false
          });
        }
      }

      totalGroupsInRound = totalGroupsInRound / 2;
      scoreCard.push(roundMatches);
    }
    return scoreCard;
  };

  static matchTaskHandler = async (
    match: RoundScoreCard,
    mIndex: number
  ): Promise<{ match: RoundScoreCard; mIndex: number }> => {
    const timeTakenByMatchToComplete = genRandomNumber(5000, 10000);
    return new Promise(resolve => {
      if (match.team1.countryCode === "BYE") {
        match.teamWonIndex = 2;
        resolve({
          match,
          mIndex
        });
      } else if (match.team2.countryCode === "BYE") {
        match.teamWonIndex = 1;
        resolve({
          match,
          mIndex
        });
      } else {
        const timeoutID = setTimeout(() => {
          const matchWonBy = genRandomNumber(1, 2);
          match.teamWonIndex = matchWonBy as -1 | 1 | 2;
          resolve({
            match,
            mIndex
          });
          clearTimeout(timeoutID);
        }, timeTakenByMatchToComplete);
      }
    });
  };
}

export default ScoreCardModel;
