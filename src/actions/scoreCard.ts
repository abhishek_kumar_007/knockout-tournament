import { Dispatch, ThunkAction } from "@reduxjs/toolkit";
import { BaseReduxAction } from "../types";
import { ScoreCardStore } from "../reducers/scoreCard";
import TeamsCommunicator from "../communicators/Teams";
import ScoreCardModel, { ScoreCard } from "../models/ScoreCard";

export interface ScoreCardStoreAction extends BaseReduxAction {
  scoreCard?: ScoreCard;
  currentRoundInProgress?: number;
  remainingRoundMatchToComplete?: number;
}

export type ThunkResult<R> = ThunkAction<
  R,
  ScoreCardStore,
  null,
  ScoreCardStoreAction
>;

export const SAVE_SCORE_CARD_INITIAL_STATE = "SAVE_SCORE_CARD_INITIAL_STATE";
export const UPDATE_SCORE_CARD = "SAVE_SCORE_CARD";
export const UPDATE_CURRENT_ROUND = "UPDATE_CURRENT_ROUND";
export const UPDATE_REMAINING_ROUND_MATCH = "UPDATE_REMAINING_ROUND_MATCH";

const teamsCommunicator = new TeamsCommunicator();

export function setInitialScoreCardState(
  scoreCard: ScoreCard,
  currentRoundInProgress: number,
  remainingRoundMatchToComplete: number
): ScoreCardStoreAction {
  return {
    type: SAVE_SCORE_CARD_INITIAL_STATE,
    scoreCard,
    currentRoundInProgress,
    remainingRoundMatchToComplete
  };
}

export function saveInitialScoreCardState(
  scoreCard: ScoreCard,
  currentRoundInProgress: number,
  remainingRoundMatchToComplete: number
): ThunkResult<Promise<ScoreCardStoreAction>> {
  return async (
    dispatch: Dispatch<ScoreCardStoreAction>
  ): Promise<ScoreCardStoreAction> => {
    return dispatch(
      setInitialScoreCardState(
        scoreCard,
        currentRoundInProgress,
        remainingRoundMatchToComplete
      )
    );
  };
}

export function updateScoreCard(
  scoreCard: ScoreCard
): ThunkResult<Promise<ScoreCardStoreAction>> {
  return async (
    dispatch: Dispatch<ScoreCardStoreAction>
  ): Promise<ScoreCardStoreAction> => {
    return dispatch({
      type: UPDATE_SCORE_CARD,
      scoreCard
    });
  };
}

export function updateCurrentRound(
  currentRound: number
): ThunkResult<Promise<ScoreCardStoreAction>> {
  return async (
    dispatch: Dispatch<ScoreCardStoreAction>
  ): Promise<ScoreCardStoreAction> => {
    return dispatch({
      type: UPDATE_CURRENT_ROUND,
      currentRoundInProgress: currentRound
    });
  };
}

export function updateRemainingRoundMatch(
  remainingRoundMatch: number
): ThunkResult<Promise<ScoreCardStoreAction>> {
  return async (
    dispatch: Dispatch<ScoreCardStoreAction>
  ): Promise<ScoreCardStoreAction> => {
    return dispatch({
      type: UPDATE_REMAINING_ROUND_MATCH,
      remainingRoundMatchToComplete: remainingRoundMatch
    });
  };
}

export function fetchTeamsData() {
  return async (dispatch: Dispatch<ScoreCardStoreAction>): Promise<void> => {
    const response = teamsCommunicator.getTeamData();
    if (response) {
      const scoreCard = ScoreCardModel.generateInitialScoreCard(
        response.length,
        response
      );
      dispatch(setInitialScoreCardState(scoreCard, 0, scoreCard[0].length));
    }
  };
}
