import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import * as actions from "./scoreCard";
import ScoreCardModel from "../models/ScoreCard";
import TeamsCommunicator from "../communicators/Teams";

const mockStore = configureMockStore([thunk]);

describe("ScoreCard actions", () => {
  it("Fetch Team data", async () => {
    const data = new TeamsCommunicator().getTeamData();

    const scoreCard = ScoreCardModel.generateInitialScoreCard(
      data.length,
      data
    );

    const expectedActions = [
      {
        type: actions.SAVE_SCORE_CARD_INITIAL_STATE,
        scoreCard,
        currentRoundInProgress: 0,
        remainingRoundMatchToComplete: scoreCard[0].length
      }
    ];

    const store = mockStore({ scoreCard: [] });

    return store
      .dispatch<any>(
        actions.saveInitialScoreCardState(scoreCard, 0, scoreCard[0].length)
      )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
