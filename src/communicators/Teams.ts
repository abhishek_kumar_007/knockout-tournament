/**
 * Teams Communicator to fetch team data
 */

export type Team = {
  teamId: number;
  countryCode: string;
  teamName: string;
};

export type TeamsApiResponse = Array<Team>;

class Teams {
  /**
   * Returning mock data for team data
   * Ideally it should be fetch from API
   */
  getMockTeamData(): TeamsApiResponse {
    return [
      {
        teamId: 1610612738,
        countryCode: "",
        teamName: "Team 1"
      },
      {
        teamId: 1610612751,
        countryCode: "",
        teamName: "Team 2"
      },
      {
        teamId: 1610612766,
        countryCode: "CHA",
        teamName: "Team 3"
      },
      {
        teamId: 1610612741,
        countryCode: "CHI",
        teamName: "Team 4"
      },
      {
        teamId: 1610612739,
        countryCode: "CLE",
        teamName: "Team 5"
      },
      {
        teamId: 1610612742,
        countryCode: "DAL",
        teamName: "Team 6"
      },
      {
        teamId: 1610612743,
        countryCode: "DEN",
        teamName: "Team 7"
      },
      {
        teamId: 1610612765,
        countryCode: "DET",
        teamName: "Team 8"
      },
      {
        teamId: 1610612744,
        countryCode: "GSW",
        teamName: "Team 9"
      }
    ];
  }

  /**
   * Fetches teams data
   */
  getTeamData = (): TeamsApiResponse => {
    return this.getMockTeamData();
  };
}

export default Teams;
