export interface StoreWithFetch {
  isFetching: boolean;
  didInvalidate: boolean;
  lastUpdated?: string | null;
}

export interface BaseReduxAction {
  type: string;
}
