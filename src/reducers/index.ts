import { combineReducers } from "@reduxjs/toolkit";
import scoreCard from "./scoreCard";

const rootReducer = () =>
  combineReducers({
    scoreCard
  });

export type RootState = ReturnType<ReturnType<typeof rootReducer>>;
export default rootReducer;
