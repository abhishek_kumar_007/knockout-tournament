import cloneDeep from "lodash/cloneDeep";
import {
  SAVE_SCORE_CARD_INITIAL_STATE,
  UPDATE_REMAINING_ROUND_MATCH,
  UPDATE_CURRENT_ROUND,
  UPDATE_SCORE_CARD,
  ScoreCardStoreAction
} from "../actions/scoreCard";
import { StoreWithFetch } from "../types";
import { ScoreCard } from "../models/ScoreCard";

export interface ScoreCardStore extends StoreWithFetch {
  scoreCard: ScoreCard;
  currentRoundInProgress: number;
  remainingRoundMatchToComplete: number;
}

export default function scoreCard(
  state: ScoreCardStore = {
    didInvalidate: false,
    isFetching: false,
    scoreCard: [],
    currentRoundInProgress: 0,
    remainingRoundMatchToComplete: 0
  },
  action: ScoreCardStoreAction
): ScoreCardStore {
  switch (action.type) {
    case SAVE_SCORE_CARD_INITIAL_STATE:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        currentRoundInProgress: action.currentRoundInProgress || 0,
        remainingRoundMatchToComplete:
          action.remainingRoundMatchToComplete || 0,
        scoreCard: cloneDeep(action.scoreCard || [])
      };
    case UPDATE_SCORE_CARD:
      return {
        ...state,
        scoreCard: cloneDeep(action.scoreCard || [])
      };
    case UPDATE_CURRENT_ROUND:
      return {
        ...state,
        currentRoundInProgress: action.currentRoundInProgress || 0
      };
    case UPDATE_REMAINING_ROUND_MATCH:
      return {
        ...state,
        remainingRoundMatchToComplete: action.remainingRoundMatchToComplete || 0
      };
    default:
      return state;
  }
}
