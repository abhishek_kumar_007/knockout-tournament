import reducer from "./scoreCard";
import ScoreCardModel from "../models/ScoreCard";
import TeamsCommunicators from "../communicators/Teams";
import { setInitialScoreCardState } from "../actions/scoreCard";

const teamsCommunicators = new TeamsCommunicators();

describe("Teams reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, { type: "" })).toEqual({
      isFetching: false,
      didInvalidate: false,
      scoreCard: [],
      currentRoundInProgress: 0,
      remainingRoundMatchToComplete: 0
    });
  });

  it("should save Teams", () => {
    const data = teamsCommunicators.getTeamData();
    const scoreCard = ScoreCardModel.generateInitialScoreCard(
      data.length,
      data
    );

    expect(
      reducer(
        undefined,
        setInitialScoreCardState(scoreCard, 0, scoreCard[0].length)
      )
    ).toEqual({
      didInvalidate: false,
      isFetching: false,
      scoreCard,
      currentRoundInProgress: 0,
      remainingRoundMatchToComplete: 8
    });
  });
});
