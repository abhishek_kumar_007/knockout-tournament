import React, { Suspense } from "react";
import { Switch, HashRouter as Router, Route } from "react-router-dom";

const KnockoutFixtures = React.lazy(() =>
  import("./pages/KnockoutFixtures/KnockoutFixtures")
);

export default function Routes() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <Switch>
          <Route exact path="/">
            <KnockoutFixtures />
          </Route>
        </Switch>
      </Router>
    </Suspense>
  );
}
