import React, { FC } from "react";
import classNames from "classnames";
import { RoundScoreCard } from "../../../models/ScoreCard";
import styles from "./match.module.scss";

export interface Props {
  matchScoreCard: RoundScoreCard;
  roundId: number;
  isFinalMatch?: boolean;
}

const Match: FC<Props> = props => {
  const { matchScoreCard, isFinalMatch, roundId } = props;

  const tournamentClassNames = classNames(styles.tournamentMatch, {
    [styles.connectorRight]: !isFinalMatch,
    [styles.finalCard]: isFinalMatch,
    [styles.connectorLeft]: roundId !== 0
  });

  const dotClassNames = classNames(styles.dot, {
    [styles.dotGreen]: matchScoreCard.isMatchInProgress,
    [styles.dotRed]: matchScoreCard.isMatchComplete
  });

  const team1ClassNames = classNames({
    [styles.winner]: matchScoreCard.teamWonIndex === 1
  });

  const team2ClassNames = classNames({
    [styles.winner]: matchScoreCard.teamWonIndex === 2
  });

  return (
    <li className={tournamentClassNames}>
      <div className={styles.tournamentMatchContainer}>
        <div>
          <div className={styles.matchTitle}>{`Match ${matchScoreCard.matchId +
            1}`}</div>
          <div className={styles.matchup}>
            <span className={team1ClassNames}>
              <div>{matchScoreCard.team1.teamName}</div>
            </span>
            <span>Vs</span>
            <span className={team2ClassNames}>
              <div>{matchScoreCard.team2.teamName}</div>
            </span>
          </div>
          <div className={styles.indicator}>
            <span className={dotClassNames} />
            {!matchScoreCard.isMatchComplete &&
            !matchScoreCard.isMatchInProgress
              ? "Pending"
              : null}
            {!matchScoreCard.isMatchComplete && matchScoreCard.isMatchInProgress
              ? "Game Started"
              : null}
            {matchScoreCard.isMatchComplete ? "Game Ended" : null}
          </div>
        </div>
      </div>
    </li>
  );
};

Match.defaultProps = {
  isFinalMatch: false
};

export default React.memo(Match);
