import React from "react";
import Enzyme from "enzyme";
import ReactDOM from "react-dom";
import Adapter from "enzyme-adapter-react-16";
import KnockoutFixtures from "./KnockoutFixtures";
import ScoreCardModel from "../../models/ScoreCard";
import TeamsCommunicator from "../../communicators/Teams";

Enzyme.configure({ adapter: new Adapter() });

const teamsCommunicator = new TeamsCommunicator();

describe("KnockoutFixtures Component", () => {
  it("Render without crash", () => {
    const div = document.createElement("div");
    const data = teamsCommunicator.getTeamData();
    ReactDOM.render(
      <KnockoutFixtures
        scoreCard={ScoreCardModel.generateInitialScoreCard(data.length, data)}
      />,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
