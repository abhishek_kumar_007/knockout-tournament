import React, { FC } from "react";
import Round from "./Round/Round";
import Match from "./Match/Match";
import { ScoreCard } from "../../models/ScoreCard";
import styles from "./knockoutFixtures.module.scss";

export interface Props {
  scoreCard: ScoreCard;
}

function getRoundsDom(scoreCard: ScoreCard) {
  const roundsDom = [];

  for (let i = 0; i < scoreCard.length; i++) {
    const round = [];

    const matches = scoreCard[i];

    let title = `Round ${i + 1}`;

    if (i === scoreCard.length - 3) {
      title = "Quarterfinals";
    } else if (i === scoreCard.length - 2) {
      title = "Semifinals";
    }
    if (i === scoreCard.length - 1) {
      title = "Finals";
    }

    round.push(
      <Round key={i} title={title}>
        {matches.map(match => {
          return (
            <Match
              key={match.matchId}
              roundId={i}
              matchScoreCard={match}
              isFinalMatch={i === scoreCard.length - 1}
            />
          );
        })}
      </Round>
    );

    roundsDom.push(round);
  }

  return roundsDom;
}

const KnockoutFixtures: FC<Props> = props => {
  const { scoreCard } = props;

  return (
    <section className={styles.knockoutFixturesSection}>
      {getRoundsDom(scoreCard)}
    </section>
  );
};

export default React.memo(KnockoutFixtures);
