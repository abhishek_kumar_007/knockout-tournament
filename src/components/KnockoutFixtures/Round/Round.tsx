import React, { FC } from "react";
import styles from "./round.module.scss";

export interface Props {
  title: string;
}

const Round: FC<Props> = ({ title, children }) => {
  return (
    <div className={styles.roundSection}>
      <h3 className={styles.title}>{title}</h3>
      <ul className={styles.roundContainer}>{children}</ul>
    </div>
  );
};

export default Round;
