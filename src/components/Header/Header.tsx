import React from "react";
import styles from "./header.module.scss";

interface Props {
  title?: string;
}

function Header({ title }: Props) {
  return (
    <div className={styles.header}>
      <div className={styles.headerContainer}>
        <div className={styles.title}>{title}</div>
      </div>
    </div>
  );
}

Header.defaultProps = {
  title: "Knockout Tournament"
};

export default Header;
