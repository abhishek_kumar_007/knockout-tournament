import React, { FC, useEffect, useRef } from "react";
import { createSelector } from "reselect";
import cloneDeep from "lodash/cloneDeep";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchTeamsData,
  updateScoreCard,
  updateCurrentRound,
  updateRemainingRoundMatch
} from "../../actions/scoreCard";
import { RootState } from "../../reducers";
import ScoreCardModel from "../../models/ScoreCard";
import { ScoreCardStore } from "../../reducers/scoreCard";
import KnockoutFixturesView from "../../components/KnockoutFixtures/KnockoutFixtures";

const selectScoreCard = createSelector<
  RootState,
  ScoreCardStore,
  ScoreCardStore
>(
  state => state.scoreCard,
  scoreCard => scoreCard
);

const KnockoutFixtures: FC = () => {
  const dispatch = useDispatch();

  const {
    scoreCard,
    currentRoundInProgress,
    remainingRoundMatchToComplete
  } = useSelector(selectScoreCard);

  const remainingMatchRef = useRef<number>(remainingRoundMatchToComplete);

  // effect to fetch team data
  useEffect(() => {
    dispatch(fetchTeamsData());
  }, [dispatch]);

  useEffect(() => {
    remainingMatchRef.current = remainingRoundMatchToComplete;
  }, [remainingRoundMatchToComplete]);

  useEffect(() => {
    const scoreCardClone = cloneDeep(scoreCard);
    let currentRound = currentRoundInProgress;
    let remainingMatch = remainingMatchRef.current;

    if (
      Array.isArray(scoreCardClone) &&
      scoreCardClone.length > currentRound &&
      remainingMatch > 0
    ) {
      const currentMatches = scoreCardClone[currentRound];
      let isDirty = false;

      currentMatches.forEach((match, index) => {
        if (!match.isMatchInProgress && !match.isMatchComplete) {
          match.isMatchInProgress = true;
          isDirty = true;

          ScoreCardModel.matchTaskHandler(match, index).then(
            ({ match: mMatch }) => {
              mMatch.isMatchComplete = true;
              mMatch.isMatchInProgress = false;

              remainingMatchRef.current = remainingMatch--;
              dispatch(updateScoreCard(scoreCardClone));

              if (
                remainingMatch <= 0 &&
                scoreCardClone.length - 1 > currentRound
              ) {
                dispatch(
                  updateScoreCard(
                    ScoreCardModel.updateScoreCard(scoreCardClone, currentRound)
                  )
                );
                dispatch(updateCurrentRound(currentRound + 1));
                remainingMatchRef.current =
                  scoreCardClone[currentRound + 1].length;
                dispatch(updateRemainingRoundMatch(remainingMatchRef.current));
              }
            }
          );
        }
      });

      if (isDirty) {
        dispatch(updateScoreCard(scoreCardClone));
      }
    }
  }, [
    dispatch,
    scoreCard,
    currentRoundInProgress,
    remainingRoundMatchToComplete
  ]);

  return <KnockoutFixturesView scoreCard={scoreCard || []} />;
};

export default KnockoutFixtures;
