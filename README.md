# N Knockout Tournament UI

The base architecture of the application is Component based Architecture and designed as SPA(Single page applications) in react.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Why React.?

- The component-based approach, well-defined lifecycle, and
  use of just plain JavaScript make React very simple to learn, build a professional web (and mobile applications), and support it.
- React uses a special syntax called JSX, which allows you to mix HTML with JavaScript.
- React creates its own virtual DOM where your components actually live. This approach gives you enormous flexibility and amazing gains in performance because React calculates what changes need to be made in the DOM beforehand and updates the DOM tree accordingly. This way, React avoids costly DOM operations and makes updates in a very efficient manner.
- React is open source and developed and maintained by Facebook and used in their internal projects.

## Libraries

- [React](https://reactjs.org/) 16
- [React-router](https://reacttraining.com/react-router/web/guides/philosophy) v4
- [TypeScript](https://www.typescriptlang.org/)
- [Scss-module](https://medium.com/@marcmintel/how-to-use-the-module-pattern-in-your-scss-sass-stylesheets-89fe38a6e1f3)
- [Webpack](https://webpack.js.org) 4
- [Babel](https://babeljs.io/) ES6/ES7 to ES5 compiler
- [Eslint](https://eslint.org/) for syntax check
- [Jest](https://flow.org/en) and Enzyme for Unit testing
- [Yarn](https://yarnpkg.com/en/) dependency manager

## Design

- **Components** :

  - Contains React presentational components which are concerned with how things look and contains only the view part.
  - Have no dependencies on the rest of the app, such as Redux actions or stores
  - Does not specify how the data is loaded or mutated.

- **Containers** :

  - Contains Container components which are concerned with how things work.
  - Provide the data and behavior to presentational or other container components.
  - Usually generated using higher order components such as connect() from React Redux.

- **Shared** :

  - Contains Reusable components which can be used across the applications.

- **types** :

  - TypeScript type definitions

- **styles** :

  - Contains all the global css/scss files and should be imported in index.ts files.

- **index.ts** :
  - Application entry point.
  - Preferred place to add all the imports needed at a global level.

## Installation

#### Prerequisite

- You need to have [Node.js](https://nodejs.org/en/download/) (> 8) installed.
- [Yarn](https://yarnpkg.com/lang/en/docs/install/) for dependency management.

#### Post Installation

If you would like to have debug capabilities, you can download these Chrome extensions

- [React DevTool](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)

## Usage

#### How to run?

```sh
Steps:
$ yarn install # if not done
$ yarn start # starts development server or
$ yarn start-prod # starts production server
```

This will start a server (using webpack).

Once you've started the server, navigate to http://localhost:3000/
to get started and view in action!

#### How to run test suit?

```sh
Steps:
 $ yarn install # if not done
 $ yarn test
```

It should print something like this:-

```
Test Suites: 8 passed, 8 total
Tests:       13 passed, 13 total
Snapshots:   0 total
Time:        8.976s
Ran all test suites related to changed files.
```
